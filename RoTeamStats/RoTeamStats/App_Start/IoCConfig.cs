﻿using Autofac;
using Autofac.Integration.WebApi;
using RoTeamStats.Core.Business;
using RoTeamStats.Core.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;

namespace RoTeamStats.App_Start
{
    public class IoCConfig
    {
        public static void RegisterDependencies()
        {
            var builder = new ContainerBuilder();

            var config = GlobalConfiguration.Configuration;
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            // Business Objects

            builder.RegisterType<AgentBO>().As<IAgentBO>().InstancePerRequest();


            // Data Access Objects



            //builder.RegisterType<TournamentTypeDA>().As<ITournamentTypeDA>().InstancePerRequest();
            builder.RegisterType<AgentDA>().As<IAgentDA>().InstancePerRequest();





            // Helpers


            // Register controllers

            //builder.RegisterType<API.TournamentController>().InstancePerRequest();
            builder.RegisterType<API.AgentController>().InstancePerRequest();


            builder.RegisterWebApiFilterProvider(config);
            builder.RegisterWebApiModelBinderProvider();




            var container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);



            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
        }
    }
}