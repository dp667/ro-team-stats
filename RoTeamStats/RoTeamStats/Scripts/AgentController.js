﻿var app = angular.module("MainApp", [])

app.controller("MainCtrl", function ($scope, $http) {

    $scope.insertAgent = function (/*agent*/) {
        var agent1 = {
            name: 'Bob',
            responses: 4,
            notes: 5
        }
        $http.post("/api/agent/insertagent", agent1, $scope.postconfig).then(function (response) {
            $scope.agent = response.data.Item;
        })

    }
    $scope.insertAgent();
});