﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(RoTeamStats.Startup))]
namespace RoTeamStats
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
