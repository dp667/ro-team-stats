﻿using RoTeamStats.Core.Business;
using RoTeamStats.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RoTeamStats.API
{
    public class AgentController : ApiController
    {
        private IAgentBO _agentBO;

        public AgentController(IAgentBO agentBO)
        {
            _agentBO = agentBO;
        }

        //[HttpGet]

        [HttpPost]
        public Agent InsertAgent (Agent agent)
        {
            return _agentBO.InsertAgent(agent);
        }
    }
}
