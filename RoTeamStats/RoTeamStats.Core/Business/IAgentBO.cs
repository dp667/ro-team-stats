﻿using RoTeamStats.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoTeamStats.Core.Business
{
    public interface IAgentBO
    {
        Agent GetAgent(int id);
        Agent InsertAgent(Agent agent);
    }
}
