﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RoTeamStats.Core.Data;
using RoTeamStats.Core.Models;

namespace RoTeamStats.Core.Business
{
    public class AgentBO : IAgentBO
    {
        private IAgentDA _agentDA;
        public AgentBO(IAgentDA agentDA)
        {
            _agentDA = agentDA;
        }
        public Agent GetAgent(int id)
        {
            return _agentDA.GetAgent(id);
        }

        public Agent InsertAgent(Agent agent)
        {
            return _agentDA.InsertAgent(agent);
        }
    }
}