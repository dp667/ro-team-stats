﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dapper;
using RoTeamStats.Core.Models;

namespace RoTeamStats.Core.Data
{
    public class AgentDA : DataConnection, IAgentDA
    {
        private const string GetAgentSQL = "GetAgent";
        private const string InsertAgentSQL = "InsertAgent";

        public Agent GetAgent(int id)
        {   
            
            using (var conn = ConnectionFactory())
            {
                conn.Open();
                var queryParams = new { AgentId = id };
                var result = conn.Query<Agent>(GetAgentSQL, param: queryParams, commandType: System.Data.CommandType.StoredProcedure);

                return result.FirstOrDefault();
            }
        }

        public Agent InsertAgent(Agent agent)
        {
            using (var conn = ConnectionFactory())
            {
                conn.Open();
                var queryParams = new { name = agent.Name, respones = agent.Responses, notes = agent.Notes };
                var result = conn.Query<int>(InsertAgentSQL, param: queryParams, commandType: System.Data.CommandType.StoredProcedure);
                if(result != null)
                {
                    agent.AgentId = result.FirstOrDefault();
                    return agent;

                }
                return null;
                
            }
        }
    }
}