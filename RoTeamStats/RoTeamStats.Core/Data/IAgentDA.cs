﻿using RoTeamStats.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RoTeamStats.Core.Data
{
    public interface IAgentDA
    {
        Agent GetAgent(int id);
        Agent InsertAgent(Agent agent);
    }
}