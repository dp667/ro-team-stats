﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RoTeamStats.Core.Models
{
    public class Agent
    {
        public string Name { get; set; }
        public int AgentId { get; set; }
        public int Responses { get; set; }
        public int Notes { get; set; }
    }
}